
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var spawn = require('child_process').spawn

var app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

app.get('/alice', function(req, res) {
  res.render('alice', { title: 'Alice' });
});

app.get('/bob', function(req, res) { res.render('bob', { title: 'Bob' }); });


app.post('/alicerun', function(req, res) {
	var ip = req.body.ip;
	var inpp = req.body.inpp;
	console.log("======run alice=======" + inpp);
	console.log("     " + ip);
	io.sockets.emit('news', { f: 'Starting Alice with Bob@ '+ip+' on input ' + inpp } ); 

	alice = spawn('/home/ubuntu/build/evl', ['../circuits/mod2dotproduct.pcf2','../inputs/evlgr.inp',ip,5001,'gg.out'])

    alice.stdout.on('data', function (data) {
                //console.log('====== ' + data);
                var sf = '===' + data;
                io.sockets.emit('news', { f: sf } ); 
    });

    alice.on('close', function (code) {
                if (code !== 0) {
                console.log('alice process exited with code ' + code);
                }
    });
});

app.post('/bobrun', function(req, res) {
	var ip = req.body.ip;
	var inpp = req.body.inpp;
	console.log("======run bob=======" + inpp);
	console.log("     " + ip);
	io.sockets.emit('news', { f: 'Starting Bob with Alice@ '+ip+' on input ' + inpp } ); 

	bob = spawn('/home/ubuntu/build/gen', ['../circuits/mod2dotproduct.pcf2','../inputs/gengr.inp',ip,5001,'gg.out'])


    bob.stdout.on('data', function (data) {
                var sf = '===' + data;
                io.sockets.emit('news', { f: sf } ); 
    });

    bob.on('close', function (code) {
                if (code !== 0) {
                console.log('alice process exited with code ' + code);
                }
    });
});


server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


io.sockets.on('connection', function (socket) {
        console.log(" ==== making connection ");
});



